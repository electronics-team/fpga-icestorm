fpga-icestorm (0~20230218gitd20a5e9-1) unstable; urgency=medium

  * New upstream version 0~20230218gitd20a5e9

 -- Daniel Gröber <dxld@darkboxed.org>  Tue, 13 Jun 2023 14:52:48 +0200

fpga-icestorm (0~20220915gita545498-4) unstable; urgency=medium

  * autopkgtest: Fix missing icetime command
  * Refresh patches
  * Add patch fixing up5k_rgb icetime failure

 -- Daniel Gröber <dxld@darkboxed.org>  Tue, 13 Jun 2023 11:56:01 +0200

fpga-icestorm (0~20220915gita545498-3) unstable; urgency=medium

  * Fix autopkgtest, examples-compile now needs nextpnr

 -- Daniel Gröber <dxld@darkboxed.org>  Wed, 16 Nov 2022 23:51:42 +0100

fpga-icestorm (0~20220915gita545498-2) unstable; urgency=medium

  * Source only upload for migration to testing

 -- Daniel Gröber <dxld@darkboxed.org>  Sun, 13 Nov 2022 23:31:00 +0100

fpga-icestorm (0~20220915gita545498-1) unstable; urgency=medium

  * New upstream version 0~20220915gita545498
  * Fix source-is-missing lintian override

 -- Daniel Gröber <dxld@darkboxed.org>  Sat, 29 Oct 2022 21:23:58 +0200

fpga-icestorm (0~20220603git2bc5417-2) unstable; urgency=medium

  * Disable autopkgtest on s390x due to missing yosys (Closes: #1016133)
  * Override lintian false-positive

 -- Daniel Gröber <dxld@darkboxed.org>  Sat, 30 Jul 2022 17:11:11 +0200

fpga-icestorm (0~20220603git2bc5417-1) unstable; urgency=medium

  * Mark can-show-help test as superficial (Closes: #974487)
  * Add autopkgtest to compile examples
  * Fix Appstream metainfo-license-invalid error
  * Add myself to Uploaders
  * New upstream version 0~20220603git2bc5417

 -- Daniel Gröber <dxld@darkboxed.org>  Sat, 18 Jun 2022 23:39:56 +0200

fpga-icestorm (0~20220102git3b7b199-3) unstable; urgency=medium

  * d/rules missed conditional

 -- Daniel Gröber <dxld@darkboxed.org>  Thu, 20 Jan 2022 02:10:51 +0100

fpga-icestorm (0~20220102git3b7b199-2) unstable; urgency=medium

  * Fix chipdb search path  - thanks, Scott! (Closes: #1003968)
  * Suggest nextpnr instead of deprecated arachne-pnr
  * Fix arch:all build (Closes: #1003984)

 -- Daniel Gröber <dxld@darkboxed.org>  Wed, 19 Jan 2022 00:18:10 +0100

fpga-icestorm (0~20220102git3b7b199-1) unstable; urgency=medium

  * Update debian/watch URL
  * New upstream version 0~20220102git3b7b199
  * Replace fragile makefile patch with dh_install
  * Remove CFLAGS/CXXFLAGS patch
  * Refresh patches
  * Fix lintian warnings
  * Update debian/copyright
  * Update debian/control
    - Fix upstream URLs
    - Update package description, more devices are supported now
    - New standards version 4.6.0 - no changes
    - R-R-R: no

 -- Daniel Gröber <dxld@darkboxed.org>  Thu, 13 Jan 2022 09:55:07 +0100

fpga-icestorm (0~20190913git0ec00d8-2) unstable; urgency=medium

  * Fix GCC-10 build with missing include (Closes: #957222)

 -- Ruben Undheim <ruben.undheim@gmail.com>  Tue, 13 Oct 2020 08:10:46 +0000

fpga-icestorm (0~20190913git0ec00d8-1) unstable; urgency=medium

  * New upstream version
  * d/patches/0004-Remove-hard-coded-path-in-icebox_vlog.py.patch:
    - Fix hard-coded erroneous path (Closes: #931071)
  * debian/control:
    - New standards version 4.4.1 - no changes
    - DH level 12
    - Use debhelper-compat

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sat, 19 Oct 2019 22:54:53 +0200

fpga-icestorm (0~20181109git9671b76-1) unstable; urgency=medium

  * New upstream GIT HEAD
  * debian/patches: Refreshed patches

 -- Ruben Undheim <ruben.undheim@gmail.com>  Thu, 20 Dec 2018 11:09:19 +0100

fpga-icestorm (0~20180904git8f61acd-1) unstable; urgency=medium

  * New upstream GIT HEAD
  * debian/copyright:
    - Added new copyright holder

 -- Ruben Undheim <ruben.undheim@gmail.com>  Fri, 07 Sep 2018 22:20:22 +0000

fpga-icestorm (0~20180809git7e73288-2) unstable; urgency=medium

  * d/patches/0003-Install-timing-data-also-in-fpga-icestorm-chipdb.patch:
    - Also install timing data from icetime into
      /usr/share/fpga-icestorm/chipdb
  * debian/control:
    - New standards version 4.2.1 - no changes

 -- Ruben Undheim <ruben.undheim@gmail.com>  Thu, 06 Sep 2018 21:47:27 +0200

fpga-icestorm (0~20180809git7e73288-1) unstable; urgency=medium

  * New upstream GIT HEAD
  * debian/compat: level 11
  * debian/control:
    - Change Maintainer to
        Debian Electronics Team <pkg-electronics-devel@lists.alioth.debian.org>
    - debhelper >= 11
    - New standards version 4.2.0 - no changes
    - "pkg-config" added as build-dependency
  * debian/tests/control and debian/tests/can-show-help:
    - Added simple autopkgtest to verify that "iceprog" can be started
      with the help command.
  * debian/upstream/metadata added
  * debian/watch:
    - Update to use git directly since upstream does not currently make regular
      releases

 -- Ruben Undheim <ruben.undheim@gmail.com>  Fri, 17 Aug 2018 13:47:54 +0200

fpga-icestorm (0~20180710git6a1bd78-1) unstable; urgency=low

  * New upstream version - refreshed patches
  * debian/control:
    - New standards version: 4.1.5 - no changes
    - fpga-icestorm-chipdb marked "Multi-Arch: foreign"
    - VCS URLs set to salsa.debian.org
  * debian/at.clifford.icestorm.metainfo.xml:
    - AppStream info for the USB connected device
  * debian/copyright: updates matching new upstream version
  * debian/rules:
    - dpkg-parsechangelog replaced with /usr/share/dpkg/pkg-info.mk

 -- Ruben Undheim <ruben.undheim@gmail.com>  Fri, 13 Jul 2018 23:36:31 +0200

fpga-icestorm (0~20160913git266e758-3) unstable; urgency=medium

  * Added 'Tag+="uaccess"' to udev rule. Thanks Petter! (Closes: #840380)

 -- Ruben Undheim <ruben.undheim@gmail.com>  Tue, 08 Nov 2016 22:45:14 +0100

fpga-icestorm (0~20160913git266e758-2) unstable; urgency=medium

  * debian/patches/0004-Make-the-build-reproducible.patch
    - Another patch for reproducibility from Chris Lamb. Thanks!
      (Closes: #840098)

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sun, 09 Oct 2016 00:59:44 +0200

fpga-icestorm (0~20160913git266e758-1) unstable; urgency=low

  * New upstream version
  * debian/man/icebram.txt:
    - New man page for new command
  * debian/patches/03_cxxflags.patch
    - Also do this for the new command

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sun, 18 Sep 2016 12:21:07 +0200

fpga-icestorm (0~20160218gitf2b2549-3) unstable; urgency=medium

  * debian/control:
    - Changed dependency on fpga-icestorm-chipdb from "Suggests:" to
      "Recommends:".
  * debian/patches/01_installpath.patch:
    - Fix hard-coded path to chipdb in icetime (Closes: #827176)
  * Updated all other patches such that they can be imported with gbp-pq.

 -- Ruben Undheim <ruben.undheim@gmail.com>  Tue, 14 Jun 2016 19:22:54 +0200

fpga-icestorm (0~20160218gitf2b2549-2) unstable; urgency=medium

  [ Daniel Shahaf ]
  * Fixed reproducibility (Closes: 823616) - thanks for the patch!

  [ Ruben Undheim ]
  * debian/control:
    - Updated Standards version to 3.9.8 - no changes

 -- Ruben Undheim <ruben.undheim@gmail.com>  Fri, 06 May 2016 20:45:23 +0200

fpga-icestorm (0~20160218gitf2b2549-1) unstable; urgency=medium

  [ Sebastian Kuzminsky ]
  * New upstream version
  * debian/man:
    - Added man pages for new commands: icetime and icepll
  * debian/patches:
    - Added patch override-CXX.patch so that the Debian build system decides
      which compiler to use.
    - Refreshed patches
  * debian/rules:
    - Set PREFIX=/usr in override_dh_auto_install

  [ Ruben Undheim ]
  * debian/control:
    - Updated Standards version to 3.9.7
  * Added debian/patches/fix_spelling_in_binaries.patch

 -- Ruben Undheim <ruben.undheim@gmail.com>  Thu, 25 Feb 2016 23:59:47 +0100

fpga-icestorm (0~20151006git103e6fd-3) unstable; urgency=medium

  * Set changelog date to UTC with LC_ALL=C to make man pages reproducible.

 -- Ruben Undheim <ruben.undheim@gmail.com>  Fri, 19 Feb 2016 17:46:30 +0100

fpga-icestorm (0~20151006git103e6fd-2) unstable; urgency=medium

  * Added udev rules so that members of the plugdev group can program
    the FPGA.
  * debian/control: Updated the Vcs-Git path so that it uses a secure protocol.

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sat, 06 Feb 2016 12:34:47 +0100

fpga-icestorm (0~20151006git103e6fd-1) unstable; urgency=low

  * Initial release (Closes: #801229)

 -- Ruben Undheim <ruben.undheim@gmail.com>  Mon, 16 Nov 2015 19:57:40 +0100
